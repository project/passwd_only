# Password Only Login (passwd_only)

The module [Password Only Login](http://drupal.org/project/passwd_only) provides a password only login form. Often authentification with username and password is too complicated. Often it is sufficient to protect some content of a drupal installation with a single password.

## How the module works

[Password Only Login](http://drupal.org/project/passwd_only) does not bypass the standard Drupal authentication system. The module needs one regular Drupal user account for its authentication. The login form of [Password Only Login](http://drupal.org/project/passwd_only) module is very similar to the normal Drupal login form except one difference: The [Password Only Login](http://drupal.org/project/passwd_only) login form transfers the username by a hidden input form field, instead of a textfield.

## Installation and setup

First you install the module, you have to select one Drupal user account. The password you set for this user is the login password in the [Password Only Login](http://drupal.org/project/passwd_only) login form.

## User protect

If you want to share the login password with several people, you have to ensure, that this password is never be changed. To accomplish this install the [User protect](http://drupal.org/project/userprotect "User protect") module. You must make the username, password and the email unchangeable.

## Caution

A user, who uses the [Password Only Login](http://drupal.org/project/passwd_only) login form, has the same rights and privileges as a user who logged in with the normal login form. You should only use this module if you know what you are doing. Never use the root user (User id 1) with this module.

## Module integration

The [Password Only Login](http://drupal.org/project/passwd_only) module integrates with the module [LoginToboggan](http://drupal.org/project/logintoboggan "LoginToboggan").

## Maintainers

Current maintainers: Josef Friedrich ([joseffriedrich](https://www.drupal.org/u/joseffriedrich))

## Testing

```
cd core
sudo -u www-data -E ../../vendor/bin/phpunit ../modules/passwd_only/tests/src/Functional
```
