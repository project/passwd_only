<?php

namespace Drupal\Tests\passwd_only\Functional;

/**
 * Test the password only login block.
 *
 * @group passwd_only
 */
class BlockTest extends PasswdOnlyWebTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'passwd_only'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer blocks',
      'access administration pages',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Test the password only login block.
   */
  public function testBlock() {
    // No block is shown.
    $this->drupalGet('');
    $this->assertSession()->responseNotContains('Password only user login');
    $this->assertSession()->responseNotContains('First create or set an user account, you want to use with the password only login module. Go to the admin page of the password only login module.');

    // Add the block.
    $block = $this->placeBlock('passwd_only_block', [
      'label' => 'Password only user login',
      'region' => 'header',
    ]);

    // Block is shown.
    $this->drupalGet('');
    $this->assertSession()->responseContains('Password only user login');
    $this->assertSession()->responseContains('First create or set an user account, you want to use with the password only login module. Go to the admin page of the password only login module.');

    // Show rendered form.
    $this->configureModule();
    $this->drupalLogout();
    $this->drupalGet('');
    $this->assertSession()->responseContains('Some description text.');
  }

}
